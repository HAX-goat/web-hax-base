var rtr = require('express').Router();

rtr.get('/', function(req, res){
   res.render('index'); 
});

module.exports = {
    router: rtr
}