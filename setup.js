// setup for hax-web-base

console.log('Setting up...');

var copyfiles = require('copyfiles');

copyfiles(["node_modules/bootstrap/dist/css/*", "public/static"], 3, callback);
copyfiles(["node_modules/bootstrap/dist/js/*", "public/static"], 3, callback);
copyfiles(["node_modules/bootstrap/dist/fonts/*", "public/static"], 3, callback);

function callback() {
    // noop
}