var router = require('./routes/router').router;
var express = require('express');
var app = express();

function startServer(serverConfObj){
    var useDefaults = (!(serverConfObj));
    
    var port = useDefaults ? 8021 : serverConfObj.port;
    app.use('/', router);
    app.use(express.static('public/static'));
    app.set('views', process.cwd() + '/public/views');
    app.set('view engine', 'ejs');
    app.listen(port, function(){
        console.log("Server up at port "+port);
    });
}

module.exports = {
    start: startServer
}